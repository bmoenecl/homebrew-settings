#!/bin/bash

# get actual dir
REPO_DIR=$(cd $(dirname $0) && pwd)

# symlink .Brewfile to ~ (remove ~/.Brewfile if it exists)
ln -sF $REPO_DIR/.Brewfile ~

# install homebrew tap 'bundle'
brew tap Homebrew/bundle

# install what is listed in brewfile
brew bundle --global --verbose
